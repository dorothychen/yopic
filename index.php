<?php

?>

<!DOCTYPE html>
<html>

<head>
    <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="index.css" type="text/css">
</head>

<body>
    <div class="message">Who wants an emoji?</div>
    <form name="form">
        <input type="text" name="username" placeholder="username"></input>
        <input type="submit" style="display: none"></input>
    </form>

    <div class="success">Success!</div>

    <div class='images'>
        <?php 
        $url = "https://raw.githubusercontent.com/tmm1/emoji-extractor/master/images/160x160/";
        for ($i=1; $i < 847; $i++) {
            echo "<img src=";
            echo '"' . $url . $i . ".png" . '"';
            echo "/>";
        }
        ?>
    </div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {
    function sendYo(user, link) {
        var http = new XMLHttpRequest();
        var url = "http://api.justyo.co/yo/";
        var params = "api_token=4caf55e9e887f626&username=" + user 
        + "&link=" + link;
        http.open("POST", url, true);

        //Send the proper header information along with the request
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        // http.setRequestHeader("Content-length", params.length);
        // http.setRequestHeader("Connection", "close");

        http.onreadystatechange = function() {//Call a function when the state changes.
            if(http.readyState == 4 && http.status == 200) {
                console.log(http.responseText);
            }
        }
        http.send(params);
        return http.responseText;
    }


    $("img").click(function() {
        var image = $(this);
        var imgsrc = image.attr("src");
        var values = $(form).serialize().split("=");

        // alert(values + values.length);
        if (values[1] == "") alert("You need a user");
        else {
            sendYo(values[1], imgsrc);
            $(".success").animate({
                opacity: 1
            }, 500, function() {
                $(this).animate({
                opacity: 0
                }, 300)
            });
        }
    });




});

</script>
</body>

</html>