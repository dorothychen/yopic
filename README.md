
### Yopic ###

A quick, easy to use webapp for sending emojis through the Yo app. Just enter your friend's username and click on the emoji you would like to send.